const signinMiddlewareTime = (request, response, next) => {
    console.log(`Method: ${request.method} - Time: ${new Date()}`);

    next();
}

const signinMiddlewareUrl = (request, response, next) => {
    console.log(`Method: ${request.method} - URL: ${request.url}`);

    next();
}

module.exports = { signinMiddlewareTime, signinMiddlewareUrl };